package be.cirb.cirbexosuivi.repositories;

import be.cirb.cirbexosuivi.models.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query("SELECT c FROM Customer c WHERE c.isActive = :active")
    List<Customer> findAllByActive(@Param("active") boolean active);
}
