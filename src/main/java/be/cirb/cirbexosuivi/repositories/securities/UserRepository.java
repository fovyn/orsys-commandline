package be.cirb.cirbexosuivi.repositories.securities;

import be.cirb.cirbexosuivi.models.entities.securities.SecurityUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<SecurityUser, Long> {
    SecurityUser findByUsername(String username);
}
