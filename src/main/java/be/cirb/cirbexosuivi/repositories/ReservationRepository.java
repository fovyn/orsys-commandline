package be.cirb.cirbexosuivi.repositories;

import be.cirb.cirbexosuivi.models.entities.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    @Query(value = "SELECT r FROM Reservation r JOIN FETCH r.customer c")
    List<Reservation> findAllBYCustomerNames(@Param("name") List<String> names);

    @Query(value = "SELECT r FROM Reservation r WHERE r.customerId = :id")
    List<Reservation> findAllByCustomerId(@Param("id") Long id);
}
