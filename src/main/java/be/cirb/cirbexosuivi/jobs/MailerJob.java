package be.cirb.cirbexosuivi.jobs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MailerJob {

    @Scheduled(cron = "0 20 * * * ?")
    public void sendMail() {
        log.info("SEND MAIL");
    }
}
