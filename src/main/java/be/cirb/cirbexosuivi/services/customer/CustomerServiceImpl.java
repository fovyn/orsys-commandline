package be.cirb.cirbexosuivi.services.customer;

import be.cirb.cirbexosuivi.models.entities.Address;
import be.cirb.cirbexosuivi.models.entities.Customer;
import be.cirb.cirbexosuivi.models.entities.Reservation;
import be.cirb.cirbexosuivi.repositories.CustomerRepository;
import be.cirb.cirbexosuivi.repositories.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
@Qualifier(value = "customer")
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository repository;
    private final ReservationRepository reservationRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository repository, ReservationRepository reservationRepository) {
        this.repository = repository;
        this.reservationRepository = reservationRepository;
    }

    @Override
    public Customer create(Customer obj) {
        return this.repository.save(obj);
    }

    @Override
    public Customer update(Long aLong, Customer obj) {
        Optional<Customer> toUpdate = this.repository.findById(aLong);
        if (toUpdate.isPresent()) {
            Customer v = toUpdate.get();
            this.merge(v, obj);

            return this.repository.save(v);
        }
        return null;
    }

    public void merge(Customer target, Customer data) {
        Arrays.stream(Customer.class.getDeclaredFields()).forEach(field -> {
            try {
                Object tValue = field.get(target);
                Object dValue = field.get(data);

                if (!tValue.equals(dValue) && dValue != null) {
                    field.set(target, dValue);
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public int delete(Long aLong) {
        Optional<Customer> opt = this.repository.findById(aLong);
        opt.ifPresent(it -> {
            it.setActive(false);
            this.repository.save(it);
        });
        return 0;
    }

    @Override
    public List<Customer> readAll() {
        return this.repository.findAllByActive(true);
    }

    @Override
    public Optional<Customer> readOne(Long aLong) {
        return this.repository.findById(aLong);
    }

    @Override
    public List<Customer> readByAddressCity(String city) {
        return null;
    }

    @Override
    public List<Customer> readByAddress(Address address) {
        return null;
    }

    public List<Reservation> getReservations(Long id) {
        return this.reservationRepository.findAllByCustomerId(id);
    }
}
