package be.cirb.cirbexosuivi.services.customer;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier(value = "admin-customer")
public class AdminCustomerServiceImpl {
}
