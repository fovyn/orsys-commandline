package be.cirb.cirbexosuivi.services.customer;

import be.cirb.cirbexosuivi.CrudService;
import be.cirb.cirbexosuivi.models.entities.Address;
import be.cirb.cirbexosuivi.models.entities.Customer;
import be.cirb.cirbexosuivi.models.entities.Reservation;

import java.util.List;

public interface CustomerService extends CrudService<Customer, Long> {
    List<Customer> readByAddressCity(String city);

    List<Customer> readByAddress(Address address);

    List<Reservation> getReservations(Long id);
}
