package be.cirb.cirbexosuivi.configs;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.crypto.SecretKey;

@ConfigurationProperties(prefix = "jwt")
@Data
public class JwtConfig {
    private String secret;
    private int expiration = 24 * 3600;
    private SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS512);

}
