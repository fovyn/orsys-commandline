package be.cirb.cirbexosuivi.controllers;

import be.cirb.cirbexosuivi.models.entities.Customer;
import be.cirb.cirbexosuivi.models.forms.home.UserForm;
import be.cirb.cirbexosuivi.models.viewmodels.customer.ListVM;
import be.cirb.cirbexosuivi.services.customer.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = {"/customer"})
@Slf4j
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping(path = {"", "/list"})
    @Secured({"USER"})
    public String listAction(Map<String, ListVM> model) {
        List<Customer> customers = this.customerService.readAll();
        ListVM vm = ListVM.builder()
                .customers(customers)
                .build();

        model.put("model", vm);

        return "customer/list";
    }

    @GetMapping(path = {"/edit/{id:[0-9]+}"})
    public String editAction(
            Map<String, Object> model,
            @PathVariable("id") Customer customer
    ) {
        log.info("Customer => {}", customer);
        model.put("customer", customer);
        return "customer/edit";
    }

    @PostMapping({"/edit"})
    public String editAction(
            Map<String, Object> model,
            @Valid UserForm form,
            BindingResult result
    ) {
        if (!result.hasErrors()) {
            Customer customer = form.toEntity();
            this.customerService.update(customer.getId(), customer);
            return "redirect:/customer/list";
        }

        Customer customer = this.customerService.readOne(form.getId()).get();
        Map<String, List<FieldError>> errors = result.getAllErrors().stream()
                .filter(it -> it instanceof FieldError)
                .map(it -> (FieldError) it)
                .collect(Collectors.groupingBy(FieldError::getField));

        model.put("errors", errors);
        return "customer/edit";
    }

    @GetMapping(path = {"/delete/{id:[0-9]+}"})
    public String deleteAction(
            @PathVariable("id") Long id
    ) {
        this.customerService.delete(id);
        return "redirect:/customer/list";
    }
}
