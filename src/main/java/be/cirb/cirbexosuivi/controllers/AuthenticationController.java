package be.cirb.cirbexosuivi.controllers;

import be.cirb.cirbexosuivi.models.forms.home.UserForm;
import be.cirb.cirbexosuivi.utils.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class AuthenticationController {
    private final AuthenticationManager authenticationManager;
    private final UserDetailsService detailsService;
    private final JwtUtil jwtUtil;

    public AuthenticationController(
            AuthenticationManager authenticationManager,
            UserDetailsService detailsService,
            JwtUtil jwtUtil) {
        this.authenticationManager = authenticationManager;
        this.detailsService = detailsService;
        this.jwtUtil = jwtUtil;
    }

    @PostMapping({"/sign-in"})
    public ResponseEntity<String> signInAction(
            @RequestBody() UserForm userForm
    ) throws Exception {
        authenticate(userForm.getFirstname(), userForm.getLastname());

        final UserDetails details = detailsService.loadUserByUsername(userForm.getFirstname());

        final String token = jwtUtil.generateToken(details);

        return ResponseEntity.ok(token);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }
}
