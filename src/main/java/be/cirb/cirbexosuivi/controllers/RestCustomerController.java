package be.cirb.cirbexosuivi.controllers;

import be.cirb.cirbexosuivi.exception.PreconditionFailedException;
import be.cirb.cirbexosuivi.models.entities.Customer;
import be.cirb.cirbexosuivi.models.entities.Reservation;
import be.cirb.cirbexosuivi.models.forms.home.UserForm;
import be.cirb.cirbexosuivi.services.customer.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping({"/api/customers"})
@CrossOrigin
public class RestCustomerController {
    private final CustomerService service;


    public RestCustomerController(CustomerService service) {
        this.service = service;
    }

    @GetMapping({""})
    public List<Customer> getAllAction() {
        return this.service.readAll();
    }

    @GetMapping({"/{id:[0-9]+}"})
    @Operation(description = "Get one customer")
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "",
                    content = {
                            @Content(schema = @Schema(implementation = Customer.class))
                    }),
//            @ApiResponse(responseCode = "412", content = {
//                    @Content(schema = @Schema(implementation = Map.class))
//            })
    })
    public ResponseEntity<Customer> getOne(
            @PathVariable("id") Long id
    ) {
        Optional<Customer> opt = this.service.readOne(id);
        if (opt.isEmpty()) {
            throw new PreconditionFailedException(Map.of("customer", "Customer cannot be found. Wrong id ?"));
//            return ResponseEntity
//                    .status(HttpStatus.PRECONDITION_FAILED)
//                    .body(Error.builder()
//                            .status(HttpStatus.PRECONDITION_FAILED.value())
//                            .message("Le client n'existe pas")
//                            .build()
//                    );
        }
        return ResponseEntity.ok(opt.get());
    }

    @PatchMapping({"/{id:[0-9]+}"})
    public Customer partialUpdate(
            @PathVariable Long id,
            @RequestBody UserForm form
    ) {
        return form.toEntity();
    }


    @GetMapping({"/{id:[0-9]+}/reservations"})
    public List<Reservation> getReservations(
            @PathVariable("id") Long id
    ) {
        return this.service.getReservations(id);
    }
}
