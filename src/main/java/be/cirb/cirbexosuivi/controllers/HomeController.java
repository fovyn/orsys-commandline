package be.cirb.cirbexosuivi.controllers;

import be.cirb.cirbexosuivi.models.forms.home.UserForm;
import be.cirb.cirbexosuivi.models.viewmodels.home.FormVM;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Controller
@Slf4j
public class HomeController {

    @RequestMapping(path = {"", "/", "index"}, method = {RequestMethod.GET})
    public String indexAction(Map<String, Object> model) {
        model.put("username", "Flavian");
        model.put("items", List.of("thibault", "Maxime", "Tristan"));
        return "home/index";
    }

    @RequestMapping(path = {"/{id:[0-9]+}"})
    public String showAction(
            Map<String, Object> model,
            @PathVariable(value = "id", required = true) int id
    ) {
        model.put("username", id);
        model.put("items", List.of());
        return "home/index";
    }

    @GetMapping(path = {"/form"})
    public String formAction(Map<String, Object> model) {
        List<FieldError> errors = List.of();
        model.put("errors", errors);
        return "home/form";
    }

    @PostMapping(path = {"/form"})
    public String formAction(Map<String, FormVM> model, @Valid UserForm body, BindingResult result) {
        FormVM.FormVMBuilder vm = FormVM.builder();
        if (result.hasErrors()) {
            result.getAllErrors().stream()
                    .filter(it -> it instanceof FieldError)
                    .map(it -> (FieldError) it)
                    .forEach(vm::error);

            model.put("model", vm.build());
            return "home/form";
        }
        log.info("Customer => {}", body);
        return "redirect:/form";
    }
}
