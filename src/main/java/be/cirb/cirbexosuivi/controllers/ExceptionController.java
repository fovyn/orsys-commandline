package be.cirb.cirbexosuivi.controllers;

import be.cirb.cirbexosuivi.exception.PreconditionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler({PreconditionFailedException.class})
    public ResponseEntity<Object> handlePrecondition(PreconditionFailedException e) {
        return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getConditions());
    }
}
