package be.cirb.cirbexosuivi;

import java.util.List;
import java.util.Optional;

public interface CrudService<TValue, TKey> {
    TValue create(TValue obj);

    TValue update(TKey key, TValue obj);

    int delete(TKey key);

    List<TValue> readAll();

    Optional<TValue> readOne(TKey key);
}
