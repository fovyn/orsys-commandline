package be.cirb.cirbexosuivi.models.viewmodels.customer;

import be.cirb.cirbexosuivi.models.entities.Customer;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;

@Data
@Builder
public class ListVM {
    @Singular
    private List<Customer> customers;
}
