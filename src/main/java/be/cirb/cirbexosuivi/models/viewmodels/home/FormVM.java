package be.cirb.cirbexosuivi.models.viewmodels.home;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.Singular;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
public class FormVM {
    @Singular
    private List<FieldError> errors = new ArrayList<>();
}
