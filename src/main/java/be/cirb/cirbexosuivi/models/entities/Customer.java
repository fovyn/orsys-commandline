package be.cirb.cirbexosuivi.models.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@EqualsAndHashCode(of = {"lastname", "firstname", "address"})
@AllArgsConstructor
@ToString
@NoArgsConstructor
@Builder
@Entity
public class Customer implements Serializable {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Getter
    @Setter
    private String lastname;

    @Getter
    @Setter
    private String firstname;

    @Getter
    @Setter
    @Embedded
    private Address address;

    @Getter
    @Setter
    private LocalDate createdAt;

    @Getter
    @Setter
    private LocalDate updatedAt;

    @Getter
    @Setter
    private boolean isActive = true;

    @Transient
    private String blop;
//    @PrePersist
//    public void prePersist() {
//        this.createdAt = LocalDateTime.now();
//    }
}
