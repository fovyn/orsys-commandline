package be.cirb.cirbexosuivi.models.entities;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Entity
public class Reservation extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate date;

    @Column(name = "customer_id", insertable = false, updatable = false)
    private Long customerId;
    @ManyToOne(targetEntity = Customer.class)
    @JoinColumn(name = "customer_id")
    @ToString.Exclude
    private Customer customer;
}
