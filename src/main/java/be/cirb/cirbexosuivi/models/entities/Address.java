package be.cirb.cirbexosuivi.models.entities;

import lombok.*;

import javax.persistence.Embeddable;

/**
 * Class that represent the live point of customer
 * FA = {streetNumber+boxNumber streetName, postalCode city, country}
 *
 * @attribute streetNumber String
 * @invariant streetNumber != null && streetNumber.size > 1
 */

@EqualsAndHashCode
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class Address {
    @NonNull
    @Getter
    @Setter
    private String streetNumber;
    @Getter
    @Setter
    private String boxNumber;
    @NonNull
    @Getter
    @Setter
    private String streetName;
    @NonNull
    @Getter
    @Setter
    private String city;
    @NonNull
    @Getter
    @Setter
    private String postalCode;
    @NonNull
    @Getter
    @Setter
    private String country;
}
