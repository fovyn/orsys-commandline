package be.cirb.cirbexosuivi.models.entities;

import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.time.LocalDate;

@Data
@MappedSuperclass
public abstract class BaseEntity {
    private LocalDate createdAt;
    private LocalDate updatedAt;
    private boolean isActive;
}
