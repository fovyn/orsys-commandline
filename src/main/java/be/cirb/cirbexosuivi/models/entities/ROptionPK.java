package be.cirb.cirbexosuivi.models.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ROptionPK implements Serializable {
    private static final long serialVersionUID = -6722551974587215396L;
    @Column(name = "reservation_id")
    private Long reservationId;
    @Column(name = "option_id")
    private Long optionId;
}
