package be.cirb.cirbexosuivi.models.entities;

import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Data
@Entity
public class ROption {
    @EmbeddedId
    private ROptionPK id;

    @ManyToOne
    @MapsId("reservation_id")
    private Reservation reservation;
    @ManyToOne
    @MapsId(value = "option_id")
    private Option option;
    private int qtt;
}
