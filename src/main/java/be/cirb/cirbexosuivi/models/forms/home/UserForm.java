package be.cirb.cirbexosuivi.models.forms.home;

import be.cirb.cirbexosuivi.models.entities.Customer;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
public class UserForm {
    private Long id;
    @NotBlank
    @NotNull(message = "errors.field.null")
    private String lastname;
    @NotNull
    @NotBlank
    @Size(min = 4)
    private String firstname;

    public static UserForm fromEntity(Customer customer) {
        return UserForm.builder()
                .firstname(customer.getFirstname())
                .id(customer.getId())
                .firstname(customer.getFirstname())
                .build();
    }

    public Customer toEntity() {
        return Customer.builder()
                .firstname(this.firstname)
                .lastname(this.lastname)
                .id(this.id)
                .build();
    }
}
