package be.cirb.cirbexosuivi.exception;

import lombok.Data;

import java.util.Map;

@Data
public class PreconditionFailedException extends RuntimeException {
    private int code = 412;
    private Map<String, String> conditions;

    public PreconditionFailedException(Map<String, String> conditions) {
        super("Precondition failed");
        this.conditions = conditions;
    }
}
