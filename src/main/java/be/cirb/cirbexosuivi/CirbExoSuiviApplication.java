package be.cirb.cirbexosuivi;

import be.cirb.cirbexosuivi.services.customer.CustomerService;
import io.swagger.v3.oas.models.annotations.OpenAPI31;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@Slf4j
@EnableScheduling
@OpenAPI31
@ConfigurationPropertiesScan
public class CirbExoSuiviApplication implements CommandLineRunner {
    private final CustomerService customerService;

    public CirbExoSuiviApplication(
            @Qualifier("customer") CustomerService customerService
    ) {
        this.customerService = customerService;
    }

    public static void main(String[] args) {
        SpringApplication.run(CirbExoSuiviApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        Customer customer = new Customer("Ovyn", "Flavian", new Address("42", "Chaussée du plouc", "Bruxelles", "1000", "Belgique"));

//        this.customerService.create(customer);
//        Customer c = new Customer();
//        c.setFirstname("Flavian");
//        c.setLastname("Ovyn");
//
//        this.customerRepository.save(c);
    }
}
