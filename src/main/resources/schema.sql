CREATE TABLE Customer
(
    customer_id      BIGINT       NOT NULL GENERATED ALWAYS AS IDENTITY,
    lastname         VARCHAR(50)  NOT NULL,
    firstname        VARCHAR(50)  NOT NULL,
    ad_street_name   VARCHAR(255) NOT NULL,
    ad_street_number VARCHAR(50)  NOT NULL,
    ad_box_number    VARCHAR(10)  NULL,
    ad_city          VARCHAR(50)  NOT NULL,
    ad_postalCode    VARCHAR(10)  NOT NULL,
    ad_country       VARCHAR(50)  NOT NULL,

    CONSTRAINT PK_Customer PRIMARY KEY (customer_id)
);

CREATE TABLE Reservation
(
    reservation_id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY,
    date           DATE   NOT NULL,
    customer_id    BIGINT NOT NULL,

    CONSTRAINT PK_Reservation PRIMARY KEY (reservation_id)
);

CREATE TABLE Option
(
    option_id   BIGINT       NOT NULL GENERATED ALWAYS AS IDENTITY,
    label       VARCHAR(255) NOT NULL,
    price       MONEY        NOT NULL,
    activity_id BIGINT       NOT NULL,

    CONSTRAINT PK_Option PRIMARY KEY (option_id)
);

CREATE TABLE Activity
(
    activity_id BIGINT       NOT NULL GENERATED ALWAYS AS IDENTITY,
    name        VARCHAR(255) NOT NULL,
    description TEXT         NULL,

    CONSTRAINT PK_Activity PRIMARY KEY (activity_id)
);

CREATE TABLE Reservation_Content
(
    reservation_id BIGINT  NOT NULL,
    option_id      BIGINT  NOT NULL,
    qtt            INTEGER NOT NULL
);

ALTER TABLE Option
    ADD CONSTRAINT FK_Option_Activity FOREIGN KEY (activity_id) REFERENCES Activity (activity_id);
ALTER TABLE Reservation
    ADD CONSTRAINT FK_Reservation_Customer FOREIGN KEY (customer_id) REFERENCES Customer (customer_id);
ALTER TABLE Reservation_Content
    ADD CONSTRAINT FK_ReservationContent_Reservation FOREIGN KEY (reservation_id) REFERENCES Reservation (reservation_id),
    ADD CONSTRAINT FK_ReservationContent_Option FOREIGN KEY (option_id) REFERENCES Option (option_id);
